from mininet.net import Mininet
from mininet.node import Controller, RemoteController, Node
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import Link, Intf

def aggNet():

	net = Mininet( topo=None, build=False)

	s1 = net.addSwitch('s1')
	h1 = net.addHost('h1', mac='00:00:00:00:00:01')
	h2 = net.addHost('h2', mac='00:00:00:00:00:02')
	h3 = net.addHost('h3', mac='00:00:00:00:00:03')

	net.addLink(s1, h1)
	net.addLink(s1, h2)
	net.addLink(s1, h3)
	net.addController(name='pox', controller= RemoteController, ip='127.0.0.1', port=6633)
	net.start()

	h1.cmd("dhclient")
	h1.cmd("ifconfig h1-eth0 10.1.1.1")
	h2.cmd("dhclient")
	h2.cmd("ifconfig h2-eth0 10.1.2.1")
	h3.cmd("dhclient")
	h3.cmd("ifconfig h3-eth0 10.1.3.1")

	CLI( net )
	net.stop()

if __name__ == '__main__': 
	setLogLevel( 'info' )
	aggNet()
